# Add NetID to AD

## Installation

### Requirements
* PHP >= 5.3

### Dependencies

```
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer
sudo apt-get install php5-ldap php5-curl
sudo a2enmod rewrite

git clone git@bitbucket.org:atomaka/add-netid.git /path/to/install
composer install
```

### Settings

```
cp src/config/settings.yml.sample
```

Update src/config/settings.yml as necessary.
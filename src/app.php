<?php

define('ENABLED_AND_NO_PASSWORD_EXPIRE', 66048);

require_once __DIR__ . '/../vendor/autoload.php';

require_once __DIR__ . '/NetID.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Toyota\Component\Ldap\Core\Manager;
use Toyota\Component\Ldap\Platform\Native\Driver;

$app = new Silex\Application();
$app['debug'] = true;

$app->register(new Silex\Provider\TwigServiceProvider(), array(
  'twig.path'     => __DIR__ . '/views',
));
$app->register(new DerAlex\Silex\YamlConfigServiceProvider(__DIR__ . '/config/settings.yml'));
$app->register(new Silex\Provider\SessionServiceProvider());

$app->before(function() use($app) {
  $app['twig']->addGlobal('layout', $app['twig']->loadTemplate('layout.html.twig'));

  $flash = $app['session']->get('flash');
  $app['session']->set('flash', null);

  if(!empty($flash)) {
    $app['twig']->addGlobal('flash', $flash);
  }
});

$app->get('/', function() use ($app) {
  // return $app['twig']->render('index.html.twig', array());
  return new RedirectResponse('new');
})->bind('homepage');

$app->get('new', function() use ($app) {
  return $app['twig']->render('new.html.twig', array());
});

$app->post('create', function(Request $request) use ($app) {
  $netid = new NetID($request->get('netid'));

  if(!$netid->isValid()) {
    $app['session']->set('flash', array(
      'type'      => 'error',
      'short'     => "Invalid NetID: {$netid->netid}.",
      'ext'       => 'The MSU NetID you entered is not valid.  Please double check the NetID and try again.  If you are certain the NetID is correct, please open a ticket to the IT Services ID Management team.'
    ));

    return new RedirectResponse('new');
  }

  // ldap stuff
  $manager = new Manager(array(
    'hostname'      => $app['config']['active-directory']['hostname'],
    'base_dn'       => $app['config']['active-directory']['base-dn']
  ), new Driver());
  $manager->connect();

  $manager->bind(
    $app['config']['active-directory']['bind-user'],
    $app['config']['active-directory']['bind-password']
  );

  $exists = true;
  try {
    $node = $manager->getNode('cn=' . $request->get('netid') . $_ENV['AD_LOCATION']);
  } catch(Exception $e) {
    $exists = false;
  }

  if(isset($node)) {
    $permissions = $node->get('userAccountControl');
  }
  if(isset($node) && $permissions[0] != ENABLED_AND_NO_PASSWORD_EXPIRE) {
    $node->get('userAccountControl')->set(array(66048));
    $manager->save($node);


      $app['session']->set('flash', array(
        'type'      => 'ok',
        'short'     => "The NetID {$netid->netid} was enabled.",
        'ext'       => 'This NetID was already listed in the Active Directory but was disabled.  It has been re-enabled.'
      ));

      return new RedirectResponse('new');
  } else {
    $active = true;
  }

  if($exists && $active) {
    $app['session']->set('flash', array(
      'type'      => 'error',
      'short'     => "The NetID {$netid->netid} already exists.",
      'ext'       => 'The MSU NetID you are attempting to import already exists and is active in the Computer Labs Active Directory.  Please open a ticket to the IT Services Classroom Software team.'
    ));

    return new RedirectResponse('new');
  }

  $fileWrite = file_get_contents($_ENV['SERVICE_URL'] . "/adduser?netid=" . $request->get('netid'));

  if($fileWrite == 0) {
     $app['session']->set('flash', array(
      'type'      => 'error',
      'short'     => "Error creating import file: {$netid->netid}.",
      'ext'       => 'There was an error importing the NetID.  Please open a ticket to the IT Services Classroom Software team.'
    ));

    return new RedirectResponse('new');
  }

  $app['session']->set('flash', array(
    'type'      => 'ok',
    'short'     => 'Import successful.',
    'ext'       => "The MSU NetID {$netid->netid} was successfully imported.  The NetID should be available for Computer labs login within the next five minutes."
  ));

  return new RedirectResponse('new');
});

$app->error(function(\Exception $e, $code) use ($app) {
  if($app['debug']) return;
  switch($code) {
    case 404:
      $message = 'The requested page could not be found.';
      break;
    default:
      $message = 'Something went wrong.';
  }

  return $app['twig']->render('error.html.twig', array(
    'code'      => $code,
    'message'   => $message,
  ));
});

return $app;

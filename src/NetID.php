<?php

class NetID {
  var $netid;

  function __construct($netid) {
    $this->netid = $netid;
  }

  function isValid() {
    $verifyApi = $$_ENV['API_URL'] . 'verify.php?netid=' . $this->netid;
    return preg_replace('/[^\d-]/', '', file_get_contents($verifyApi)) == 1;
  }
}
